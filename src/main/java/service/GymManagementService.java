package service;

import exceptions.NoScheduleAvailableException;
import exceptions.NoSuchGymClassException;
import model.*;
import repository.*;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

public class GymManagementService {

    ClientMembershipRepository clientMembershipRepository = new ClientMembershipRepository();
    ClientRepository clientRepository = new ClientRepository();
    MembershipTypeRepository membershipTypeRepository = new MembershipTypeRepository();
    ScheduleRepository scheduleRepository = new ScheduleRepository();
    RegistrationRepository registrationRepository = new RegistrationRepository();
    GymClassRepository gymClassRepository = new GymClassRepository();



    public ClientMembership saveClientMembership(Client client, MembershipType membershipType) {

        if (clientRepository.getById(client.getId()) == null || membershipTypeRepository.getById(membershipType.getId()) == null) {
            throw new RuntimeException("Client ID or Membership type does not exist");
        }

        LocalDate date = LocalDate.now();

        List<ClientMembership> clientMemberships = clientMembershipRepository.getAllByClientId(client.getId());

        if (clientMemberships.size() == 0) {
            ClientMembership clientMembership = new ClientMembership(membershipType, client, date, date.plusDays(30));
            return clientMembershipRepository.save(clientMembership);
        } else {
            for (ClientMembership c : clientMemberships) {
                if (c.getEndDate().isEqual(date) || c.getEndDate().isBefore(date)) {
                    ClientMembership clientMembership = new ClientMembership(membershipType, client, date, date.plusDays(30));
                    return clientMembershipRepository.save(clientMembership);
                } else {
                    throw new RuntimeException("Membership is still valid please try after" + c.getEndDate());
                }
            }
        }
        return null;

    }

    public void displayScheduleForDay(LocalDate localDate) {

        List<Schedule> scheduleByDay = scheduleRepository.getScheduleByDay(localDate);
        for (Schedule s : scheduleByDay) {
            System.out.println("-------------------------");
            System.out.println("Class: " + s.getGymClass().getClassName());
            System.out.println("Trainer: " + s.getTrainer().getName() + " " + s.getTrainer().getSurName());
            System.out.println("Starting at: " + s.getStartDate().toLocalDateTime().getHour() + ":" + s.getStartDate().toLocalDateTime().getMinute());
            System.out.println("Ending at: " + s.getEndDate().toLocalDateTime().getHour() + ":" + s.getEndDate().toLocalDateTime().getMinute());
            System.out.println("-------------------------");
        }

    }

    public void addRegistrationToClassForClient(LocalDate dayToSearch, Client client, GymClass gymClass) throws NoSuchGymClassException, NoScheduleAvailableException {


        if (clientRepository.getById(client.getId()) == null) {
            throw new RuntimeException("Client ID or Membership type does not exist");
        }

        LocalDate date = LocalDate.now();

        List<ClientMembership> clientMemberships = clientMembershipRepository.getAllByClientId(client.getId());

        if (clientMemberships.size() <= 0){
            throw new RuntimeException("Please make a new membership to make a new registration");
        }
        for (ClientMembership c : clientMemberships) {
            if (c.getEndDate().isEqual(date) || c.getEndDate().isBefore(date)) {
                throw new RuntimeException("Your membership is not valid! \n Please update your membership! ");
            }

            List<Schedule> scheduleByDay = scheduleRepository.getScheduleByDay(dayToSearch);
            if (scheduleByDay.size() > 0) {
                for (Schedule s : scheduleByDay) {
                    if (s.getGymClass().getClassName().equalsIgnoreCase(gymClass.getClassName())) {
                        registrationRepository.save(new Registration(client, s));
                    }
                }
            } else {
                throw new NoScheduleAvailableException("There is no schedule available for this date: " + dayToSearch);
            }


        }

    }

    public GymClass getClassByName(String gymClassName) {

        List<GymClass> gymClassList = gymClassRepository.getAll();
        for (GymClass g : gymClassList) {
            if (g.getClassName().equalsIgnoreCase(gymClassName)) {
                return g;
            }
        }
        return null;
    }


}



