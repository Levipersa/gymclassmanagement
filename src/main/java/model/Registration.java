package model;

import javax.persistence.*;

@Entity
@Table(name = "registration")
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
    @ManyToOne
    @JoinColumn(name = "schedule_id")
    private Schedule schedule;

    public Registration() {
    }

    public Registration(Integer id, Client client, Schedule schedule) {
        this.id = id;
        this.client = client;
        this.schedule = schedule;
    }

    public Registration(Client client, Schedule schedule) {
        this.client = client;
        this.schedule = schedule;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                ", client=" + client +
                ", schedule=" + schedule +
                '}';
    }
}
