package model;

import javax.persistence.*;

@Entity
@Table(name = "membership_type")
public class MembershipType {
    //Id, membership_name, membership_cost, description
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "membership_name")
    private String membershipName;
    @Column(name = "membership_cost")
    private String membershipCost;
    private String description;

    public MembershipType() {
    }

    public MembershipType(Integer id, String membershipName, String membershipCost, String description) {
        this.id = id;
        this.membershipName = membershipName;
        this.membershipCost = membershipCost;
        this.description = description;
    }

    public MembershipType(String membershipName, String membershipCost, String description) {
        this.membershipName = membershipName;
        this.membershipCost = membershipCost;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMembershipName() {
        return membershipName;
    }

    public void setMembershipName(String membershipName) {
        this.membershipName = membershipName;
    }

    public String getMembershipCost() {
        return membershipCost;
    }

    public void setMembershipCost(String membershipCost) {
        this.membershipCost = membershipCost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "MembershipType{" +
                "id=" + id +
                ", membershipName='" + membershipName + '\'' +
                ", membershipCost='" + membershipCost + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
