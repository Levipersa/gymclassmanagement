package model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;

@Entity
@Table(name = "client_membership")
public class ClientMembership {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name="membership_id")
    private MembershipType membershipType;
    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;
    @Column(name = "start_date")
    private LocalDate startDate;
    @Column(name = "end_date")
    private LocalDate endDate;

    public ClientMembership() {
    }

    public ClientMembership(Integer id, MembershipType membershipType, Client client, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.membershipType = membershipType;
        this.client = client;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public ClientMembership(MembershipType membershipType, Client client, LocalDate startDate, LocalDate endDate) {
        this.membershipType = membershipType;
        this.client = client;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MembershipType getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(MembershipType membershipType) {
        this.membershipType = membershipType;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void LocalDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "ClientMembership{" +
                "id=" + id +
                ", membershipType=" + membershipType +
                ", client=" + client +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
