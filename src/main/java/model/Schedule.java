package model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "schedule")
public class Schedule {
    // Id, class_id, trainer_id, start date, end date
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "class_id")
    private GymClass gymClass;
    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private Trainer trainer;
    @Column(name = "start_date")
    private Timestamp startDate;
    @Column(name = "end_date")
    private Timestamp endDate;
    public Schedule() {
    }

    public Schedule(Integer id, GymClass gymClass, Trainer trainer, Timestamp startDate, Timestamp endDate) {
        this.id = id;
        this.gymClass = gymClass;
        this.trainer = trainer;
        this.startDate = startDate;
        this.endDate = endDate;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GymClass getGymClass() {
        return gymClass;
    }

    public void setGymClass(GymClass gymClass) {
        this.gymClass = gymClass;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", classId=" + gymClass +
                ", trainerId=" + trainer +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
