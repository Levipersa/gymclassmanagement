package model;

import javax.persistence.*;

@Entity
@Table(name = "classes")
public class GymClass {
    //Id, class_name, description
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "class_name")
    private String className;
    private String description;

    public GymClass(){

    }

    public GymClass(Integer id, String className, String description) {
        this.id = id;
        this.className = className;
        this.description = description;
    }

    public GymClass(String className, String description) {
        this.className = className;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GymClass{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

