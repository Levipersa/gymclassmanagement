package utils;

import model.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtils {

    private static final String DATABASE_HOST = "jdbc:mysql://localhost:3306/gym_sda_levente";
    private static final String DATABASE_USERNAME = "root";
    private static final String DATABASE_PASSWORD = "root";

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {

            Configuration myConfiguration = new Configuration();

            Properties settings = new Properties();
            settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
            settings.put(Environment.URL, DATABASE_HOST);
            settings.put(Environment.USER, DATABASE_USERNAME);
            settings.put(Environment.PASS, DATABASE_PASSWORD);
            settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
            settings.put(Environment.SHOW_SQL, "false");
            myConfiguration.setProperties(settings);
            myConfiguration.addAnnotatedClass(Client.class);
            myConfiguration.addAnnotatedClass(ClientMembership.class);
            myConfiguration.addAnnotatedClass(GymClass.class);
            myConfiguration.addAnnotatedClass(MembershipType.class);
            myConfiguration.addAnnotatedClass(Registration.class);
            myConfiguration.addAnnotatedClass(Schedule.class);
            myConfiguration.addAnnotatedClass(Trainer.class);


            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(myConfiguration.getProperties()).build();

            sessionFactory = myConfiguration.buildSessionFactory(serviceRegistry);

        }
        return sessionFactory;
    }
}
