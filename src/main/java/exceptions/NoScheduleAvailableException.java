package exceptions;

public class NoScheduleAvailableException extends RuntimeException {

    public NoScheduleAvailableException() {
        super();
    }

    public NoScheduleAvailableException(String message) {
        super(message);
    }

    public NoScheduleAvailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoScheduleAvailableException(Throwable cause) {
        super(cause);
    }

    protected NoScheduleAvailableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
