package exceptions;

public class NoSuchGymClassException extends RuntimeException {

    public NoSuchGymClassException() {
        super();
    }

    public NoSuchGymClassException(String message) {
        super(message);
    }

    public NoSuchGymClassException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchGymClassException(Throwable cause) {
        super(cause);
    }

    protected NoSuchGymClassException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
