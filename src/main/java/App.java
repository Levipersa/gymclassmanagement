import exceptions.NoScheduleAvailableException;
import exceptions.NoSuchGymClassException;
import model.*;
import repository.*;
import service.GymManagementService;
import utils.HibernateUtils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class App {


    public static void main(String[] args) {

        ClientMembershipRepository clientMembershipRepository = new ClientMembershipRepository();
        MembershipTypeRepository membershipTypeRepository = new MembershipTypeRepository();
        ClientRepository clientRepository = new ClientRepository();
        GymManagementService gymManagementService = new GymManagementService();
        GymClassRepository gymClassRepository = new GymClassRepository();
        ScheduleRepository scheduleRepository = new ScheduleRepository();
        TrainerRepository trainerRepository = new TrainerRepository();
        RegistrationRepository registrationRepository = new RegistrationRepository();
//        clientRepository.save(new Client("Pop","Marius","pop@yahoo.com","0755666123"));
//        clientRepository.save(new Client("Darius","Mozambic","darius@yahoo.com","0744666123"));
//        clientRepository.save(new Client("Attila","Hun","hun@yahoo.com","0766444231"));
//        membershipTypeRepository.save(new MembershipType("Platinum", "100LEI",
//                "Acces to all Club Locations \n" +
//                        "Workout 24hours/7days \n" +
//                        "Free fitness orientation"));
//
//        membershipTypeRepository.save(new MembershipType("Gold", "70LEI",
//                "Acces to all Club Locations \n" +
//                        "Workout 24hours/5days \n"));
//
//        membershipTypeRepository.save(new MembershipType("Silver", "50LEI",
//                "Acces to all Club Locations \n" +
//                        "Workout 24hours/3days \n"));
//
//
//        gymManagementService.saveClientMembership(clientRepository.getById(1),membershipTypeRepository.getById(1));
//
//        gymClassRepository.save(new GymClass("TRX",
//                "This workout covers every major muscle group utilizing TRX straps at a fast pace format."));
//
//        gymClassRepository.save(new GymClass("Cycling",
//                "This 45-minute class is perfect for those new to indoor cycling and/or building up fitness, but still takes care of business with an excellent hard workout."));
//
//        gymClassRepository.save(new GymClass("Aerobic",
//                " A great workout for all levels of fitness enthusiasts, this class combines cardiovascular training and toning exercises, for a superior total body workout."));

        //        scheduleRepository.save(new Schedule(gymClassRepository.getById(2),trainerRepository.getById(1),startDate,endDate));
//                scheduleRepository.save(new Schedule(gymClassRepository.getById(3),trainerRepository.getById(1),startDate,endDate));

        Timestamp startDate = Timestamp.valueOf("2020-08-09 16:00:00.0");
        Timestamp endDate = Timestamp.valueOf("2020-08-09 18:30:00.0");

        Timestamp dayToSearch = Timestamp.valueOf("2020-08-09 17:30:15.0");
        LocalDate d = dayToSearch.toLocalDateTime().toLocalDate();


//        gymManagementService.displayScheduleForDay(LocalDate.of(2020, 8, 10));

        String trx = "trx";
        String cycling = "Cycling";
        String aerobic = "Aerobic";

//
//        clientMembershipRepository.save(new ClientMembership(membershipTypeRepository.getById(2),
//                clientRepository.getById(2),LocalDate.now(),LocalDate.now().plusDays(30)));
//
//            gymManagementService.addRegistrationToClassForClient(LocalDate.of(2020, 8, 10),
//                    clientRepository.getById(2),gymManagementService.getClassByName(cycling));


        System.out.println(registrationRepository.getAll());



    }
}
