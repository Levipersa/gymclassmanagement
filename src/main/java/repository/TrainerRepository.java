package repository;

import model.Trainer;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class TrainerRepository {

    public List<Trainer> getAll(){
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllTrainers = session.createQuery("from Trainer");

        List<Trainer> trainers = selectAllTrainers.list();

        session.close();

        return trainers;

    }


    public Trainer getById(Integer trainerId) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Trainer trainerFound = session.find(Trainer.class, trainerId);
        session.close();
        return trainerFound;
    }

    public void save(Trainer trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(trainer);

        transaction.commit();
        session.close();
    }

    public void update(Trainer trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(trainer);

        transaction.commit();
        session.close();
    }

    public void delete(Trainer trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(trainer);

        transaction.commit();
        session.close();
    }

}
