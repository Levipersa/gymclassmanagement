package repository;

import model.Schedule;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

public class ScheduleRepository {

    public List<Schedule> getAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllSchedules = session.createQuery("from ClientMembership");

        List<Schedule> registrations = selectAllSchedules.list();

        session.close();

        return registrations;
    }

    public Schedule getById(Integer scheduleId) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Schedule scheduleFound = session.find(Schedule.class, scheduleId);

        session.close();

        return scheduleFound;
    }

    public void save(Schedule schedule) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.save(schedule);

        transaction.commit();
        session.close();

    }

    public void update(Schedule schedule) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.update(schedule);

        transaction.commit();
        session.close();

    }

    public void delete(Schedule schedule) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.delete(schedule);

        transaction.commit();
        session.close();

    }

    public List<Schedule> getScheduleByDay(LocalDate localDate) {


        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectScheduleByDay = session.createQuery("from Schedule s WHERE s.startDate >= :dayToSearch and s.startDate < :nextDay ");

        Date startDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(localDate.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());

        selectScheduleByDay.setParameter("dayToSearch", startDate);
        selectScheduleByDay.setParameter("nextDay", endDate);

        List<Schedule> schedules = selectScheduleByDay.list();

        session.close();

        return schedules;

    }


}
