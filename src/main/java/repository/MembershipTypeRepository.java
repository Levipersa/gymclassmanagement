package repository;

import model.MembershipType;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class MembershipTypeRepository {

    public List<MembershipType> getAll(){
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllMemberships = session.createQuery("from MembershipType");

        List<MembershipType> membershipTypes = selectAllMemberships.list();

        session.close();

        return membershipTypes;
    }

    public MembershipType getById(Integer membershipID) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        MembershipType membershipFound = session.find(MembershipType.class, membershipID);

        session.close();
        return membershipFound;
    }

    public void save(MembershipType membershipType) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(membershipType);

        transaction.commit();
        session.close();
    }

    public void update(MembershipType membershipType) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(membershipType);

        transaction.commit();
        session.close();
    }

    public void delete(MembershipType membershipType) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(membershipType);

        transaction.commit();
        session.close();
    }



}
