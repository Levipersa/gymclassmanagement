package repository;

import model.GymClass;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class GymClassRepository {

    public List<GymClass> getAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllGymClasses = session.createQuery("from GymClass");

        List<GymClass> gymClasses = selectAllGymClasses.list();

        session.close();

        return gymClasses;
    }

    public GymClass getById(Integer gymClassId) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        GymClass gymClass = session.find(GymClass.class, gymClassId);

        session.close();
        return gymClass;
    }



    public void save(GymClass gymClass){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(gymClass);

        transaction.commit();
        session.close();

    }

    public void update(GymClass gymClass){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(gymClass);

        transaction.commit();
        session.close();

    }


    public void delete(GymClass gymClass){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(gymClass);

        transaction.commit();
        session.close();

    }




}
