package repository;

import model.ClientMembership;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.time.LocalDate;
import java.util.List;

public class ClientMembershipRepository {

    public List<ClientMembership> getAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllClientMembership = session.createQuery("from ClientMembership");

        List<ClientMembership> registrations = selectAllClientMembership.list();

        session.close();

        return registrations;
    }

    public ClientMembership getById(Integer clientMembershipId) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        ClientMembership registrationFound = session.find(ClientMembership.class, clientMembershipId);

        session.close();

        return registrationFound;
    }

    public ClientMembership save(ClientMembership clientMembership) {

        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.save(clientMembership);

        transaction.commit();
        session.close();

        return clientMembership;
    }


    public void delete(ClientMembership clientMembership) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.delete(clientMembership);

        transaction.commit();
        session.close();

    }

    public List<ClientMembership> getAllByClientId(Integer clientId) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllMembersById = session.createQuery("from ClientMembership c WHERE c.client.id = :targetClient ");

        selectAllMembersById.setParameter("targetClient", clientId);

        List<ClientMembership> clientMemberships = selectAllMembersById.list();

        session.close();

        return clientMemberships;


    }
}
