package repository;

import model.Registration;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class RegistrationRepository {

    public List<Registration> getAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllRegistrations = session.createQuery("from Registration");

        List<Registration> registrations = selectAllRegistrations.list();

        session.close();

        return registrations;
    }

    public Registration getById(Integer registrationId) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Registration registrationFound = session.find(Registration.class, registrationId);

        session.close();

        return registrationFound;
    }

    public void save(Registration registration) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.save(registration);

        transaction.commit();
        session.close();

    }

    public void update(Registration registration) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.update(registration);

        transaction.commit();
        session.close();

    }

    public void delete(Registration registration) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        session.delete(registration);

        transaction.commit();
        session.close();

    }


}
