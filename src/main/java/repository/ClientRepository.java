package repository;

import model.Client;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class ClientRepository {


    public List<Client> getAll(){
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllClients = session.createQuery("from Client");

        List<Client> clients = selectAllClients.list();

        session.close();

        return clients;

    }

    public Client getById(Integer clientId) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Client clientFound = session.find(Client.class, clientId);
        session.close();
        return clientFound;
    }

    public Client save(Client client) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(client);

        transaction.commit();
        session.close();

        return client;
    }

    public void update(Client client) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(client);

        transaction.commit();
        session.close();
    }

    public void delete(Client client) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(client);

        transaction.commit();
        session.close();
    }





}
